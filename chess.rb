require 'debugger'
require 'colorize'

class Game

  attr_accessor :board

  def initialize
    @board = Board.new
  end

  def play
    turns = [:white, :black]
    until game_over?
      board.display_board

      print "It is #{turns.first}'s turn."

      begin
        board.move(get_start_move(turns), get_end_move) #([x,y],[x,y])
      rescue
        puts "Please enter a different move: "
        retry
      end
      turns.reverse!
    end

    game_over_display
  end

  def game_over_display
    puts "Game over!"
    if board.checkmate?(:white) && board.checked?(:white)
      puts "Black won!"
    elsif board.checkmate?(:black) && board.checked?(:black)
      puts "White won!"
    else
      puts "Stalemate! It's a draw!"
    end
  end

  def get_start_move(turn)
    print "What piece do you want to move?: "
    input = gets.chomp

    start_pos = convert_coordinates(input[1].to_i, input[0])
    correct_player_move?(start_pos, turn)
    start_pos
  end

  def get_end_move
    print "Where do you want to move it?: "
    input = gets.chomp

    convert_coordinates(input[1].to_i, input[0])
  end

  def game_over?
    board.checkmate?(:white) || board.checkmate?(:black)
  end

  def correct_player_move?(input_pos, turn)
    if board[input_pos].color != turn.first
      raise ArgumentError.new "This is #{turn.first.to_s}s piece!"
    end
  end

  def convert_coordinates(x, y)
    mapped_x = (x-8).abs
    mapped_y = "abcdefgh".index(y)
    [mapped_x, mapped_y]
  end

end

class Board
  DISPLAY_KEY = {
    :rook => { :white => "\u2656", :black => "\u265C" },
    :knight => { :white => "\u2658", :black => "\u265E" },
    :bishop => { :white => "\u2657", :black => "\u265D" },
    :queen => { :white => "\u2655", :black => "\u265B" },
    :king => { :white => "\u2654", :black => "\u265A" },
    :pawn => { :white => "\u2659", :black => "\u265f" }
  }

  attr_accessor :board, :pieces

  def initialize
    @board = Array.new(8) { Array.new(8) }
    @pieces = []
    set_board
  end

  def [](pos)
    @board[pos.first][pos.last]
  end

  def []=(pos, value)
    @board[pos.first][pos.last] = value
  end

  def set_board
    starting_piece_sets = [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook]
    starting_piece_sets.each_with_index do |piece_class, index|
      set_piece([7, index], piece_class, :white)
    end

    (0..7).each do |index|
      set_piece([6, index], Pawn, :white)
    end

    starting_piece_sets.each_with_index do |piece_class, index|
      set_piece([0, index], piece_class, :black)
    end

    (0..7).each do |index|
      set_piece([1, index], Pawn, :black)
    end
  end

  def set_piece(pos, piece_class, color)
    new_piece = piece_class.new(pos, color, self)
    x, y = pos[0], pos[1]
    self.board[x][y] = new_piece
    self.pieces << new_piece
  end

  def find_king(color)
    pieces.select do |piece|
      piece.color == color && piece.class == King
    end.first
  end

  def checked?(color)
    color_king = find_king(color)

    pieces.any? { |piece| piece.moves.include?(color_king.position) }
  end

  def move!(start_pos, end_pos)
    move(start_pos, end_pos, false)
  end

  def move(start_pos, end_pos, check_valid = true)
    moving_piece, end_piece = self[start_pos], self[end_pos]

    check_move_exception(start_pos, end_pos, check_valid)

    unless end_piece.nil?
      pieces.delete(end_piece)
      end_piece.position = nil
    end

    self[end_pos], self[start_pos] = moving_piece, nil
    moving_piece.position = end_pos
  end

  def check_move_exception(start_pos, end_pos, check_valid)
    moving_piece = self[start_pos]
    raise ArgumentError.new "Start position is invalid!" if moving_piece.nil?

    unless moving_piece.moves.include?(end_pos)
      raise ArgumentError.new "End position is invalid!"
    end

    if check_valid && moving_piece.move_into_check?(end_pos)
      raise ArgumentError.new "Invalid move!  This will leave moving player in check"
    end
  end

  def checkmate?(color)
    checkmate = true
    pieces.each do |piece|
      if piece.color == color
        checkmate = false unless piece.valid_moves.empty?
      end
    end

    checkmate
  end

  def dup
    dup_board = Board.new
    dup_board.board = Array.new(8) { Array.new(8) }
    dup_board.pieces = []

    pieces.each do |piece|
      dup_piece = piece.dup(dup_board)
      dup_board[piece.position.dup] = dup_piece
      dup_board.pieces << dup_piece
    end

    dup_board
  end

  def display_board
    index = 1
    board.each_with_index do |row, row_index|
      print (row_index-8).abs
      row.each do |square|
        if index % 2 == 0
          print "   ".colorize(:background => :light_black) if square.nil?
          print " #{DISPLAY_KEY[square.class.to_s.downcase.to_sym][square.color]} ".colorize(:background => :light_black)  if square
        else
          print "   ".colorize(:background => :light_white) if square.nil?
          print " #{DISPLAY_KEY[square.class.to_s.downcase.to_sym][square.color]} ".colorize(:background => :light_white) if square
        end
        index += 1
      end
      index += 1
      puts
    end
    print "  a  b  c  d  e  f  g  h"
    puts
    nil
  end
end

class Piece
  attr_reader :board, :color, :position

  def initialize(position, color, board)
    @position = position
    @color = color
    @board = board
  end

  def dup(dup_board)
    self.class.new(position.dup, color, dup_board)
  end

  def position=(pos)
    @position = pos
  end

  def move_into_check?(pos)
    dup_board = board.dup
    dup_board.move!(position, pos)
    dup_board.checked?(color)
  end

  def valid_moves
    possible_moves = self.moves
    possible_moves.delete_if do |move|
      move_into_check?(move)
    end
    possible_moves
  end

  def on_board?(coordinates)
    (0..7).include?(coordinates[0]) && (0..7).include?(coordinates[1])
  end

end

class SlidingPiece < Piece

  def moves
    get_coords(self.move_dirs)
  end

  def get_coords(directions)
    possible_moves = []
    shift = 1
    until directions.empty? # [[-1,-1],[-1,1],[1,-1],[1,1]]
      directions.each do |direction|
        shifted_dist = [direction.first * shift, direction.last * shift]
        possible_move = [position.first + shifted_dist.first, position.last + shifted_dist.last]

        if !on_board?(possible_move)
          directions.delete(direction)
        elsif board[possible_move] # on board and not nil
          directions.delete(direction)
          possible_moves << possible_move unless board[possible_move].color == color
        else
          possible_moves << possible_move
        end
      end

      shift += 1
    end

    possible_moves
  end
end

class Bishop < SlidingPiece

  def move_dirs
    [[-1,-1], [-1,1], [1,-1], [1,1]]
  end

end

class Rook < SlidingPiece

  def move_dirs
    [[1,0],[0,1],[-1,0],[0,-1]]
  end
end

class Queen < SlidingPiece

  def move_dirs
    [[-1,-1],[-1,1], [1,-1], [1,1],[1,0],[0,1],[-1,0],[0,-1]]
  end
end

class SteppingPiece < Piece

  def moves
    possible_moves = []

    self.move_deltas.each do |delta|
      move = [position[0] + delta[0], position[1] + delta[1]]
      if on_board?(move)
        possible_moves << move if !(occupied_by_us?(move))
      end
    end

    possible_moves
  end

  def occupied_by_us?(coordinates)
    !board[coordinates].nil? && board[coordinates].color == color
  end
end

class King < SteppingPiece

  def move_deltas
    [[-1,-1], [-1,0], [-1,1], [0,-1], [0,1], [1,-1], [1,0], [1,1]]
  end

end

class Knight < SteppingPiece

  def move_deltas
    [[-1,-2], [-1,2], [1,-2], [1,2], [-2,-1], [-2,1], [2,-1], [2,1]]
  end
end

class Pawn < Piece

  attr_accessor :first_move

  def initialize(position, color, board)
    super(position, color, board)
    @first_move = true
  end

  def moves
    possible_moves = []
    forward_shift = -1 if color == :white
    forward_shift = 1 if color == :black

    forward_move = [position[0]+forward_shift,position[1]]
    left_diag_move = [position[0]+forward_shift,position[1]-1]
    right_diag_move = [position[0]+forward_shift,position[1]+1]

    possible_moves << forward_move if check_forward(forward_move)
    possible_moves << left_diag_move if check_diag(left_diag_move)
    possible_moves << right_diag_move if check_diag(right_diag_move)

    if first_move
      two_step_move = [position[0]+(forward_shift*2),position[1]]
      possible_moves << two_step_move if check_forward(two_step_move) && check_forward(forward_move)
    end

    possible_moves
  end

  def position=(pos)
    super(pos)
    first_move = false
  end

  def check_forward(move)
    board[move].nil? && on_board?(move)
  end

  def check_diag(move)
    !board[move].nil? && board[move].color != color
  end
end